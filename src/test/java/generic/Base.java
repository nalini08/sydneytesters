package generic;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class Base {

	public WebDriver driver = null;
	public Logger log = null;
	public String url = null;
	private String className = null;

	public Base() {
		className = this.getClass().getName();
	}

	@BeforeMethod(alwaysRun = true)
	public void browserLaunch() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		driver.get(url);
		logger("Initializing driver, Launching " + url, "info", className);
	}

	@AfterMethod(alwaysRun = true)
	public void tear_down() {
		driver.quit();
		logger("Driver tear down, " + url, "info", className);
	}

	public void logger(String msg, String msgType, String className) {
		PropertyConfigurator.configure(System.getProperty("user.dir") + "/src/test/resources/log4j.properties");
		log = Logger.getLogger(className);
		if (msgType.equalsIgnoreCase("info"))
			log.info(msg);
		else if (msgType.equalsIgnoreCase("debug"))
			log.debug(msg);
		else if (msgType.equalsIgnoreCase("error"))
			log.error(msg);
		else if (msgType.equalsIgnoreCase("fatal"))
			log.fatal(msg);
		Reporter.log(className + ":" + msg, false);
	}
}
