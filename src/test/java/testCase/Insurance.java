package testCase;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import generic.Base;
import pageFactory.CarInsurancePage;
import pageFactory.InsuranceHomePage;
import pageFactory.LifeInsurancePage;

public class Insurance extends Base {
	public InsuranceHomePage homePage = null;
	public CarInsurancePage carPage = null;
	public LifeInsurancePage lifePage = null;
	private String className = null;

	public Insurance() {
		url = "http://sydneytesters.herokuapp.com/";
		className = this.getClass().getName();
	}

	@BeforeMethod
	public void initPages() {
		homePage = new InsuranceHomePage(driver);
		carPage = new CarInsurancePage(driver);
		lifePage = new LifeInsurancePage(driver);
	}

	@Test
	@Parameters({ "carMake", "carYear", "driverAge", "driverGender", "regState", "driverEmail" })
	public void carInsurence(String carMake, String carYear, String driverAge, String driverGender, String regState,
			String driverEmail) {
		logger("Validating insurance home page", "info", className);
		if (homePage.validatePageBanner().equalsIgnoreCase("Sydney Testers Insurance")) {
			logger("Insurance home page loaded successfully.", "info", className);
		} else {
			logger("Insurance home page loaded failed.", "error", className);
			Assert.fail();
		}
		logger("Launching car quote.", "info", className);
		homePage.clickCarQuote();
		logger("Validating car insurance page", "info", className);
		if (carPage.validatePageBanner().equalsIgnoreCase("Sydney Testers Car Insurance")) {
			logger("Car insurance home page loaded successfully.", "info", className);
		} else {
			logger("Car insurance home page loaded failed.", "error", className);
			Assert.fail();
		}
		logger("Setting car make: " + carMake, "info", className);
		carPage.setCarMake(carMake);
		logger("Setting car make year: " + carYear, "info", className);
		carPage.setCarYear(carYear);
		logger("Setting driver age: " + driverAge, "info", className);
		carPage.setDriverAge(driverAge);
		logger("Setting driver gender: " + driverGender, "info", className);
		carPage.setDriverGender(driverGender);
		logger("Setting car reg state: " + regState, "info", className);
		carPage.setCarState(regState);
		logger("Setting driver email: " + driverEmail, "info", className);
		carPage.setDriverEmail(driverEmail);
		logger("Retriving car quote.", "info", className);
		carPage.getCarInsuranceQuote();
		logger("Car Premium: " + carPage.getPremium(), "info", className);
	}

	@Test
	@Parameters({ "lifeAge", "lifeGender", "lifeOccupation", "lifeState", "lifeEmail", })
	public void lifeInsurence(String lifeAge, String lifeGender, String lifeOccupation, String lifeState,
			String lifeEmail) {
		logger("Validating insurance home page", "info", className);
		if (homePage.validatePageBanner().equalsIgnoreCase("Sydney Testers Insurance")) {
			logger("Insurance home page loaded successfully.", "info", className);
		} else {
			logger("Insurance home page loaded failed.", "error", className);
			Assert.fail();
		}
		logger("Launching life quote.", "info", className);
		homePage.clickLifeQuote();
		logger("Validating life insurance page", "info", className);
		if (lifePage.validatePageBanner().equalsIgnoreCase("Sydney Testers Life Insurance")) {
			logger("Life insurance home page loaded successfully.", "info", className);
		} else {
			logger("Life insurance home page loaded failed.", "error", className);
			Assert.fail();
		}
		logger("Setting client age " + lifeAge, "info", className);
		lifePage.setClientAge(lifeAge);
		logger("Setting client gender: " + lifeGender, "info", className);
		lifePage.setclientGender(lifeGender);
		logger("Setting  client occupation: " + lifeOccupation, "info", className);
		lifePage.setClientOccupation(lifeOccupation);
		logger("Setting client email: " + lifeEmail, "info", className);
		lifePage.setClientEmail(lifeEmail);
		logger("Retriving life quote.", "info", className);
		lifePage.getLifeInsuranceQuote();
		logger("Life Premium: " + lifePage.getPremium(), "info", className);
	}
}