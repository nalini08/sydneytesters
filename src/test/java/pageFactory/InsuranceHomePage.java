package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsuranceHomePage {

	@FindBy(id = "getcarquote")
	WebElement getCarQuoteButton;

	@FindBy(id = "getlifequote")
	WebElement getLifeQuoteButton;

	@FindBy(className = "text-center")
	WebElement insurancePageBanner;

	public InsuranceHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void clickCarQuote() {
		getCarQuoteButton.click();
	}

	public void clickLifeQuote() {
		getLifeQuoteButton.click();
	}

	public String validatePageBanner() {
		String homePageBanner = insurancePageBanner.getText();
		return homePageBanner;
	}
}