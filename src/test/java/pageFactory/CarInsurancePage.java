package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CarInsurancePage {
	@FindBy(id = "make")
	WebElement carMakeList;

	@FindBy(id = "year")
	WebElement carMakeYear;

	@FindBy(id = "age")
	WebElement carDriverAge;

	@FindBy(id = "female")
	WebElement carDriverGenderFemale;

	@FindBy(xpath = "male")
	WebElement carDriverGenderMale;

	@FindBy(id = "state")
	WebElement carState;

	@FindBy(id = "email")
	WebElement carEmail;

	@FindBy(id = "getquote")
	WebElement carGetQuote;

	@FindBy(className = "text-center")
	WebElement carInsurancePageBanner;

	@FindBy(className = "mark")
	WebElement carPremium;

	public CarInsurancePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void setCarMake(String carMake) {
		Select make_list = new Select(carMakeList);
		make_list.selectByVisibleText(carMake);
	}

	public void setCarYear(String carYear) {
		carMakeYear.sendKeys(carYear);
	}

	public void setDriverAge(String driverAge) {
		carDriverAge.sendKeys(driverAge);
	}

	public void setDriverGender(String driverGender) {
		if (driverGender.equalsIgnoreCase("male")) {
			carDriverGenderMale.click();
		} else {
			carDriverGenderFemale.click();
		}
	}

	public void setCarState(String regState) {
		Select state = new Select(carState);
		state.selectByVisibleText(regState);
	}

	public void setDriverEmail(String driverEmail) {
		carEmail.sendKeys(driverEmail);
	}

	public void getCarInsuranceQuote() {
		carGetQuote.click();
	}

	public String validatePageBanner() {
		String carPageBanner = carInsurancePageBanner.getText();
		return carPageBanner;
	}

	public String getPremium() {
		String driverPremium = carPremium.getText();
		return driverPremium;
	}
}
