package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class LifeInsurancePage {
	@FindBy(id = "age")
	WebElement clientAge;

	@FindBy(id = "female")
	WebElement clientGenderFemale;

	@FindBy(id = "male")
	WebElement clientGenderMale;

	@FindBy(id = "occupation")
	WebElement clientOccupation;

	@FindBy(id = "email")
	WebElement clientEmail;

	@FindBy(id = "getquote")
	WebElement lifeGetQuote;

	@FindBy(id = "state")
	WebElement clientState;

	@FindBy(className = "text-center")
	WebElement lifeInsurancePageBanner;

	@FindBy(className = "mark")
	WebElement clientPremium;

	public LifeInsurancePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public void setClientAge(String lifeAge) {
		clientAge.sendKeys(lifeAge);
	}

	public void setclientGender(String lifeGender) {
		if (lifeGender.equalsIgnoreCase("male")) {
			clientGenderMale.click();
		} else {
			clientGenderFemale.click();
		}
	}

	public void setClientOccupation(String lifeOccupation) {
		Select make_list = new Select(clientOccupation);
		make_list.selectByVisibleText(lifeOccupation);
	}

	public void setClientState(String lifeState) {
		Select state = new Select(clientState);
		state.selectByVisibleText(lifeState);
	}

	public void setClientEmail(String lifeEmail) {
		clientEmail.sendKeys(lifeEmail);
	}

	public void getLifeInsuranceQuote() {
		lifeGetQuote.click();
	}

	public String validatePageBanner() {
		String lifePageBanner = lifeInsurancePageBanner.getText();
		return lifePageBanner;
	}

	public String getPremium() {
		String lifePremium = clientPremium.getText();
		return lifePremium;
	}
}
